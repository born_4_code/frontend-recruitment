Cześć,

w repozytorium znajduje się zadanie rekutacyjne na stanowisko Junior Frontend Developer w Born4Code.

Aby poprawnie wykonać zadanie pamiętaj o poniższych rzeczach:

1. Zrób fork repozytorium
2. Wykonaj zadanie
3. Przetestuj poprawność swojego rozwiązania
4. Zrób commit na swoje repozytorium
5. Podeślij informację o wykonaniu na adres: rekrutacja@born4code.pl

Na wykonanie zadania masz 60 minut licząc od momentu otrzymania od nas e-maila.

Powodzenia!
